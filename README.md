# Navigation #

This is the first project of [Deep Reinforcement Learning Nanodegree](https://www.udacity.com/course/deep-reinforcement-learning-nanodegree--nd893) by Udacity. 

For this project, We train an agent to navigate and collect bananas in a large, square world.

![Navigate and collect banana](image/banana.gif)

A reward of +1 is provided for collecting a yellow banana, and a reward of -1 is provided for collecting a blue banana. Thus, the goal of the agent is to collect as many yellow bananas as possible while avoiding blue bananas.

The state space has 37 dimensions and contains the agent's velocity, along with ray-based perception of objects around the agent's forward direction. Given this information, the agent has to learn how to best select actions. Four discrete actions are available, corresponding to:

0. move forward.
1. move backward.
2. turn left.
3. turn right.

The goal of this project is that the agent must get an average score of +13 over 100 consecutive episodes.

### File Structure ###
```
- model.py # the neural network model as function approximator
- dqn_agent.py # Deep Q-learning algorithm code
- checkpoint.pth # weights of trained neural network model
- Navigation.ipynb # notebook to train the agent
- README.md
```

### Library ###
1. numpy 1.12.1
2. matplotlib 2.1.0
3. torch 0.4.0

### Dependencies ###

To set up your python environment to run the code in this repository, follow the instructions below.

1. Create (and activate) a new environment with Python 3.6.

	- __Linux__ or __Mac__: 
	```bash
	conda create --name drlnd python=3.6
	source activate drlnd
	```
	- __Windows__: 
	```bash
	conda create --name drlnd python=3.6 
	activate drlnd
	```
	
2. Follow the instructions in [this repository](https://github.com/openai/gym) to perform a minimal install of OpenAI gym.  
	- Next, install the **classic control** environment group by following the instructions [here](https://github.com/openai/gym#classic-control).
	- Then, install the **box2d** environment group by following the instructions [here](https://github.com/openai/gym#box2d).
	
3. Clone the repository (if you haven't already!), and navigate to the `python/` folder.  Then, install several dependencies.
```bash
git clone https://github.com/udacity/deep-reinforcement-learning.git
cd deep-reinforcement-learning/python
pip install .
```

4. Create an [IPython kernel](http://ipython.readthedocs.io/en/stable/install/kernel_install.html) for the `drlnd` environment.  
```bash
python -m ipykernel install --user --name drlnd --display-name "drlnd"
```

5. Before running code in a notebook, change the kernel to match the `drlnd` environment by using the drop-down `Kernel` menu. 

![](image/kernel.PNG)

### Instruction ###
For this project, we will not need to install Unity - this is because we can download the ready-to-use environment from one of the links below. You need only select the environment that matches your operating system:

- Linux: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Linux.zip)
- Mac OSX: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana.app.zip)
- Windows (32-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Windows_x86.zip)
- Windows (64-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Windows_x86_64.zip)

Then, place the files in this repository.

After that, you can open `Navigation.ipynb` and train the agent there.

If you would like to change the neural network model, you can change `model.py`. And if you want to improve the algorithm, you can modify `dqn_agent.py`.

### Results  ###
The agent can achieve average score of +13 after 521 episodes. 

![Navigate and collect banana](image/reward_plot.PNG)

### Acknowledgement ###
1. [Udacity](https://www.udacity.com/)
2. [Unity](https://unity.com/)